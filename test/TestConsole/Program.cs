﻿using System;
using System.Diagnostics;
using FsCms.Entity;
using FsCms.Service;
using FsCms.Service.DAL;

namespace TestConsole
{
    class Program
    {
        public static IFreeSql mysql = new FreeSql.FreeSqlBuilder()
          .UseConnectionString(FreeSql.DataType.MySql, "Data Source=127.0.0.1;Port=3307;User ID=root;Password=abc123456;Initial Catalog=FsCms;Charset=utf8;SslMode=none;Max pool size=10")
          .UseAutoSyncStructure(true)

          .UseMonitorCommand(
              cmd =>
              {
                  Trace.WriteLine(cmd.CommandText);
              }, //监听SQL命令对象，在执行前
              (cmd, traceLog) =>
              {
                  Console.WriteLine(traceLog);
              }) //监听SQL命令对象，在执行后
          .UseLazyLoading(true)
          .Build();

        static void Main(string[] args)
        {
            var list = mysql.Select<ArticleContent>().Where(w => w.Id > 0).ToList();
            Console.WriteLine("获取的记录条数："+list.Count);
            Console.ReadKey();
        }
    }
}
