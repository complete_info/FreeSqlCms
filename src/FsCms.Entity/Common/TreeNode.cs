﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FsCms.Entity.Common
{
    public class TreeNode
    {
        public string id { get; set; }

        public string pid { get; set; }

        public string name { get; set; }
    }
}
