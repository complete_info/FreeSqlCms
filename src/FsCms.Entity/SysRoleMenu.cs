﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FsCms.Entity
{
    public class SysRoleMenu : BaseEntity
    {
        [Display(Name = "角色ID")]
        public int RoleId { get; set; }

        [Display(Name = "菜单ID")]
        public int MenuId { get; set; }

        [ForeignKey("MenuId")]
        public SysMenu Menu { get; set; }

        [ForeignKey("RoleId")]
        public SysRole Role { get; set; }
    }
}


